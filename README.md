# First things first #
To make your life easier, if you haven't already, it is recommended you have the following in `$HOME/.asetup`:
```
[defaults]
briefprint      = True
autorestore     = True
```
This will allow you to ommit using "here" when setting up `Athena`, and will eliminate the need to specify the release each time (i.e. calling `asetup` will read the `.asetup.save` in your working directory and fall back to `$HOME/.asetup.save` if there isn't one).
# Setup on `lxplus` or other `cvmfs`-equipped node#
```
$ mkdir build source run && cd build
$ setupATLAS && acmSetup --sourcedir=../source 21.2.51,AnalysisBase
```
Subsequent setups require only:
```
$ cd build && setupATLAS && acmSetup
```
without the `acmSetup` arguments (unless you are changing releases).

# Get the code #
```
$ acm clone_project DijetFramework dijetpluslepton/DijetFramework
```
This will clone and checkout the `master` branch.

# Developing #
If you are just running the code, it's fine to stay on `master`. However, if you're going to be developing or making changes to the code, you'll need to create your own branch (since we don't want people modifying `master` directly, but instead through merge requests). Create your own branch based on `master`, e.g.:
```
$ cd DijetFramework
$ git checkout -b vpascuzz-dev origin/master
```
and submit merge requests (MR) once you have validated the changes.

# Compilation #
First, add the project packages you want to compile
```
$ acm add_pkg DijetFramework/xAODAnaHelpers DijetFramework/DijetResonanceAlgo
```
then compile:
```
$ acm compile
```

# Running the code #
## Local tests ##
For *local* testing, you will need to specify the input file to use. For `data18`:
```
$ INPUT=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/lepdijet/data/data18_13TeV.00358395.physics_Main.deriv.DAOD_STDM4.f961_m2015_p3583/DAOD_STDM4.15759686._000584.pool.root.1
```
For `MC16a` signal (W'/Z' sample):
```
$INPUT=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/lepdijet/mc/mc16_13TeV.308488.Pythia8EvtGen_A14NNPDF23LO_Wprime1250_Zprime1000.deriv.DAOD_STDM4.e5914_e5984_s3126_r9364_r9315_p3554/DAOD_STDM4.15316605._000001.pool.root.1
```

and run:
```
$ cd ../run
$ xAH_run.py --files $INPUT \
--config $DijetFramework_DIR/data/DijetResonanceAlgo/CONFIG.py \
-f direct
```
where `CONFIG.py` is either:
```
config_data_Nominal_lepton.py
```
or
```
config_SignalSyst_mc16a.py
```
depending on if you're running on the `data18` or `MC16a` test `INPUT`.

If you need to run over many local files, then use `--inputList` option. Here is an example running over all data DAOD files:
```
ls -d -1 [directory with DAOD]/* > data.in
xAH_run.py --files data.in --inputList --config $DijetFramework_DIR/data/DijetResonanceAlgo/CONFIG.py -f direct
``` 

## GRID submission ##
Once you're setup:
```
$ lsetup rucio
$ localSetupPandaClient
$ runGridSubmission.py [runOn]
```
where `runOn` is one of:
```
data15, data16, data17, data18, mc16{a,d}_stdm4_{qcdpythia,ttbar,zpjets,singletop,wpjets}
```

## Batch (`condor`) submission ##
Some modifications to the scripts are required (much of what should be variable-ised is hard-coded, for now):
1. `cd` into your `run` directory and create symlinks (`ln -s`) to the files `runCondor*`, which are located in the `scripts` directory.
2. In `runCondorSubmission.sh`, change the variables `C_ROOTDIR` (where you have your `source`, `build`, and `run` directories) and `C_RUNDIR` (where you want to call `xAH_run.py` from).
3. In `runCondorSubmission.sub`, change the `MC_TYPE` (`tc16` or `ssm16`) and `RUN#` (directories with the `ROOT` files).
4. `condor_submit runCondorSubmission.sub`

# Building the docker image #
Once you've cloned the project, either as described above using `acm` or using:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/dijetpluslepton/DijetFramework.git
```
and you have Docker installed, building the image is simple:
```
$ cd DijetFramework
$ docker build . -f ci/Dockerfile
Sending build context to Docker daemon  163.6MB
Step 1/10 : FROM atlas/analysisbase:21.2.51
 ---> ec2476e44a58
Step 2/10 : MAINTAINER Vincent R. Pascuzzi "vpascuzz@cern.ch"
...
Successfully built 26c56774eadc
```

# TODO #
- RECAST.

# Current Issues #
None!
