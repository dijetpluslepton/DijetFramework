#
# Environment configuration file setting up the installed DijetFramework project.
#

# Set up the base environment using the base image's setup script:
echo ""
echo "###################################################"
echo "Setting up analysis release..."
source release_setup.sh

# Set up the DijetFramework installation:
echo "Setting up DijetFramework..."
source /usr/DijetFramework/*/InstallArea/*/setup.sh
echo "Configured DijetFramework from: ${DijetFramework_DIR}"
echo "Exporting environment variables..."
export WorkDir_DIR=$DijetFramework_DIR  # Needed to find 'data' folder
export DijetFramework_CONFIGS=$WorkDir_DIR/data/DijetResonanceAlgo/config
echo "Done!"
echo "###################################################"

# Set up the prompt:
export PS1='\[\033[01;35m\][bash]\[\033[01;31m\][\u DijetFramework-$DijetFramework_VERSION]\[\033[01;34m\]:\W >\[\033[00m\] ';

