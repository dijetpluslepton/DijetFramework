#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODAnaHelpers/HelperFunctions.h"

#include "DijetResonanceAlgo/MiniTree.h"

MiniTree :: MiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, xAOD::TStore* store /* = 0 */) :
  HelpTreeBase(event, tree, file, 1e3)
{
  m_event = event;
  Info("MiniTree", "Creating output TTree");
  m_firstEvent = true;
  m_store = store;
}

MiniTree :: ~MiniTree()
{
}
//////////////////// Connect Defined variables to branches here /////////////////////////////
void MiniTree::AddEventUser(const std::string /*detailStr*/)
{
  // event variables
  m_tree->Branch("yStar",  &m_yStar,  "yStar/F");
  m_tree->Branch("yBoost", &m_yBoost, "yBoost/F");
  m_tree->Branch("mjj",  &m_mjj,  "mjj/F");
  m_tree->Branch("pTjj", &m_pTjj, "pTjj/F");
  m_tree->Branch("m3j", &m_m3j, "m3j/F");
  m_tree->Branch("deltaPhi", &m_deltaPhi, "deltaPhi/F");

  // Muons
  m_tree->Branch("muon_absCBqOverP", &m_absCBqOverP, "muon_absCBqOverP/F");
  m_tree->Branch("muon_sigmaCBqOverP", &m_sigmaCBqOverP, "muon_sigmaCBqOverP/F");

  // punch-through
  m_tree->Branch("Insitu_Segs_response_E", &m_Insitu_Segs_response_E, "Insitu_Segs_response_E/F");
  m_tree->Branch("Insitu_Segs_response_pT", &m_Insitu_Segs_response_pT, "Insitu_Segs_response_pT/F");
  m_tree->Branch("punch_type_segs", &m_punch_type_segs, "punch_type_segs/I");

  // pT balance - for cleaning
  m_tree->Branch("pTBalance", &m_pTbalance, "pTBalance/F");
  m_tree->Branch("MHT",       &m_MHT,       "MHT/F");
  m_tree->Branch("MHTPhi",    &m_MHTPhi,    "MHTPhi/F");
  m_tree->Branch("MHTJVT",    &m_MHTJVT,    "MHTJVT/F");
  m_tree->Branch("MHTJVTPhi", &m_MHTJVTPhi, "MHTJVTPhi/F");

  // weights
  m_tree->Branch("weight", &m_weight, "weight/F");
  m_tree->Branch("weight_xs", &m_weight_xs, "weight_xs/F");
  m_tree->Branch("weight_prescale", &m_weight_prescale, "weight_prescale/F");
  m_tree->Branch("weight_resonanceKFactor", &m_weight_resonanceKFactor, "weight_resonanceKFactor/F");

}

void MiniTree::AddJetsUser(const std::string /*detailStr*/, const std::string /*jetName*/)
{
  m_tree->Branch("jet_constitScaleEta", &m_jet_constitScaleEta);
  m_tree->Branch("jet_emScaleE", &m_jet_emScaleE);
  m_tree->Branch("jet_emScaleEta", &m_jet_emScaleEta);
  m_tree->Branch("jet_emScalePhi", &m_jet_emScalePhi);
  m_tree->Branch("jet_minDeltaR", &m_jet_minDeltaR);
  m_tree->Branch("jet_numberCloseJets", &m_jet_numberCloseJets);

  //reclustered jets
  m_tree->Branch("jet_recluster_pt", &m_jet_recluster_pt);
  m_tree->Branch("jet_recluster_eta", &m_jet_recluster_eta);
  m_tree->Branch("jet_recluster_phi", &m_jet_recluster_phi);
  m_tree->Branch("jet_recluster_e", &m_jet_recluster_e);
}

//////////////////// Clear any defined vectors here ////////////////////////////
void MiniTree::ClearEventUser() {
  m_yStar    = -999;
  m_yBoost   = -999;
  m_mjj      = -999;
  m_pTjj     = -999;
  m_m3j      = -999;
  m_deltaPhi = -999;

  // Muons
  m_absCBqOverP   = -999.;
  m_sigmaCBqOverP = -999.;

  m_Insitu_Segs_response_E = -999;
  m_Insitu_Segs_response_pT = -999;
  m_punch_type_segs = -999;
  m_pTbalance = -999;
  m_MHT      = -999;
  m_MHTJVT   = -999;
  m_MHTPhi   = -999;
  m_MHTJVTPhi= -999;

  m_weight_corr = -999;
  m_weight    = -999;
  m_weight_xs = -999;
  m_weight_prescale = -999;
  m_weight_resonanceKFactor = -999;
}

void MiniTree::ClearJetsUser(const std::string /*jetName*/) {
  m_jet_constitScaleEta.clear();
  m_jet_emScaleE.clear();
  m_jet_emScaleEta.clear();
  m_jet_emScalePhi.clear();
  m_jet_minDeltaR.clear();
  m_jet_numberCloseJets.clear();

  //reclustered jets
  m_jet_recluster_pt.clear();
  m_jet_recluster_eta.clear();
  m_jet_recluster_phi.clear();
  m_jet_recluster_e.clear();
}

/////////////////// Assign values to defined event variables here ////////////////////////
void MiniTree::FillEventUser( const xAOD::EventInfo* eventInfo ) {

  if( eventInfo->isAvailable< float >( "yStar" ) )
    m_yStar = eventInfo->auxdecor< float >( "yStar" );
  if( eventInfo->isAvailable< float >( "yBoost" ) )
    m_yBoost = eventInfo->auxdecor< float >( "yBoost" );

  if( eventInfo->isAvailable< float >( "mjj" ) )
    m_mjj = eventInfo->auxdecor< float >( "mjj" );
  if( eventInfo->isAvailable< float >( "pTjj" ) )
    m_pTjj = eventInfo->auxdecor< float >( "pTjj" );
  if( eventInfo->isAvailable< float >( "m3j" ) )
    m_m3j = eventInfo->auxdecor< float >( "m3j" );
  if( eventInfo->isAvailable< float >( "deltaPhi" ) )
    m_deltaPhi = eventInfo->auxdecor< float >( "deltaPhi" );

  // Muons
  if( eventInfo->isAvailable< float >( "muon_absCBqOverP" ) )
    m_absCBqOverP = eventInfo->auxdecor< float >( "muon_absCBqOverP" );
  if( eventInfo->isAvailable< float >( "muon_sigmaCBqOverP" ) )
    m_sigmaCBqOverP = eventInfo->auxdecor< float >( "muon_sigmaCBqOverP" );

  if( eventInfo->isAvailable< float >( "weight" ) )
    m_weight = eventInfo->auxdecor< float >( "weight" );
  if( eventInfo->isAvailable< float >( "weight_xs" ) )
    m_weight_xs = eventInfo->auxdecor< float >( "weight_xs" );
  if( eventInfo->isAvailable< float >( "weight_prescale" ) )
    m_weight_prescale = eventInfo->auxdecor< float >( "weight_prescale" );
  if( eventInfo->isAvailable< float >( "weight_resonanceKFactor" ) )
    m_weight_resonanceKFactor = eventInfo->auxdecor< float >( "weight_resonanceKFactor" );

  if( eventInfo->isAvailable< float >( "Insitu_Segs_response_E" ) )
    m_Insitu_Segs_response_E = eventInfo->auxdecor< float >( "Insitu_Segs_response_E" );
  if( eventInfo->isAvailable< float >( "Insitu_Segs_response_pT" ) )
    m_Insitu_Segs_response_pT = eventInfo->auxdecor< float >( "Insitu_Segs_response_pT" );
  if( eventInfo->isAvailable< int >( "punch_type_segs" ) )
    m_punch_type_segs = eventInfo->auxdecor< int >( "punch_type_segs" );

  if( eventInfo->isAvailable< float >( "pTBalance" ) )
    m_pTbalance = eventInfo->auxdecor< float >( "pTBalance" );

  if( eventInfo->isAvailable< float >( "MHT" ) )
    m_MHT = eventInfo->auxdecor< float >( "MHT" );
  if( eventInfo->isAvailable< float >( "MHTPhi" ) )
    m_MHTPhi = eventInfo->auxdecor< float >( "MHTPhi" );
  if( eventInfo->isAvailable< float >( "MHTJVT" ) )
    m_MHTJVT = eventInfo->auxdecor< float >( "MHTJVT" );
  if( eventInfo->isAvailable< float >( "MHTJVTPhi" ) )
    m_MHTJVTPhi = eventInfo->auxdecor< float >( "MHTJVTPhi" );

}

/////////////////// Assign values to defined jet variables here //////////////////
void MiniTree::FillJetsUser( const xAOD::Jet* jet, const std::string /*jetName*/ ) {
  if( jet->isAvailable< float >( "constitScaleEta" ) ) {
    m_jet_constitScaleEta.push_back( jet->auxdata< float >("constitScaleEta") );
  } else {
    m_jet_constitScaleEta.push_back( -999 );
  }
  if( jet->isAvailable< float >( "emScaleE" ) ) {
    m_jet_emScaleE.push_back( jet->auxdata< float >("emScaleE") );
  } else {
    m_jet_emScaleE.push_back( -999 );
  }
  if( jet->isAvailable< float >( "emScaleEta" ) ) {
    m_jet_emScaleEta.push_back( jet->auxdata< float >("emScaleEta") );
  } else {
    m_jet_emScaleEta.push_back( -999 );
  }
  if( jet->isAvailable< float >( "emScalePhi" ) ) {
    m_jet_emScalePhi.push_back( jet->auxdata< float >("emScalePhi") );
  } else {
    m_jet_emScalePhi.push_back( -999 );
  }

  if( jet->isAvailable< float >( "minDeltaR" ) ) {
    m_jet_minDeltaR.push_back( jet->auxdata< float >("minDeltaR") );
  } else {
    m_jet_minDeltaR.push_back( -999 );
  }

  if( jet->isAvailable< int >( "numberCloseJets" ) ) {
    m_jet_numberCloseJets.push_back( jet->auxdata< int >("numberCloseJets") );
  } else {
    m_jet_numberCloseJets.push_back( -999 );
  }

  //reclustered jets
  if( jet->isAvailable< float >( "recluster_pt" ) ) {
    m_jet_recluster_pt.push_back( jet->auxdata< float >("recluster_pt") / 1e3 );
  } else {
    m_jet_recluster_pt.push_back( -999 );
  }
  if( jet->isAvailable< float >( "recluster_eta" ) ) {
    m_jet_recluster_eta.push_back( jet->auxdata< float >("recluster_eta") );
  } else {
    m_jet_recluster_eta.push_back( -999 );
  }
  if( jet->isAvailable< float >( "recluster_phi" ) ) {
    m_jet_recluster_phi.push_back( jet->auxdata< float >("recluster_phi") );
  } else {
    m_jet_recluster_phi.push_back( -999 );
  }
  if( jet->isAvailable< float >( "recluster_e" ) ) {
    m_jet_recluster_e.push_back( jet->auxdata< float >("recluster_e") / 1e3 );
  } else {
    m_jet_recluster_e.push_back( -999 );
  }

}

void MiniTree::AddBtag(std::string bTagWPNames) {
    std::stringstream ss(bTagWPNames);
    std::string thisWPString;

    // Split list of WPs by commas, and make vector placeholders //
    while (std::getline(ss, thisWPString, ',')) {
      m_bTagWPs.push_back( thisWPString );
      if (thisWPString.find("Fixed") != std::string::npos)
        m_bTagIsFixed.push_back(true);
      else
        m_bTagIsFixed.push_back(false);

      std::string thisNum = thisWPString.substr( thisWPString.find_last_of('_')+1, thisWPString.size() );
      m_bTagWPNums.push_back(thisNum);

      m_mbb.push_back( -1. );
      m_mbj.push_back( -1. );
      std::vector<float>  tmpVec;
      m_weight_btag.push_back( tmpVec );
    }  

    // Connect vector placeholders to WPs //
    for( unsigned int iB=0; iB < m_bTagWPs.size(); ++iB){
      std::string baseBTagType = "";
      if (m_bTagIsFixed.at(iB))
        baseBTagType = "fix_";
      else
        baseBTagType = "flt_";

      std::string thisBTagName = "";

      thisBTagName = "mbb_"+baseBTagType+m_bTagWPNums.at(iB)+m_bTagWPNums.at(iB);
      m_tree->Branch(thisBTagName.c_str(), &(m_mbb.at(iB)), (thisBTagName+"/F").c_str() );  

      thisBTagName = "mbj_"+baseBTagType+m_bTagWPNums.at(iB)+m_bTagWPNums.at(iB);
      m_tree->Branch(thisBTagName.c_str(), &(m_mbj.at(iB)), (thisBTagName+"/F").c_str() );  

      thisBTagName = "weight_btag_"+baseBTagType+m_bTagWPNums.at(iB);
      m_tree->Branch(thisBTagName.c_str(), &(m_weight_btag.at(iB)) );  
    }
  
    m_tree->Branch("systSF_btag_names",      &m_systSF_btag_names    );

}
/////////////////// Add variables for the b-tagged di-jet analysis //////////////////
void MiniTree::FillBtag( const xAOD::EventInfo* eventInfo ) {
  this->ClearBtag();

  // Find and fill relevant varaibles for all WPs //
  for( unsigned int iB=0; iB < m_bTagWPs.size(); ++iB){
    std::string baseBTagType = "";
    if (m_bTagIsFixed.at(iB))
      baseBTagType = "FixedCutBEff_";
    else
      baseBTagType = "FlatBEff_";

    std::string thisBTagName = "";

    thisBTagName = "mbb_"+baseBTagType+m_bTagWPNums.at(iB);
    if( eventInfo->isAvailable< float >( thisBTagName.c_str() ) ){
      m_mbb.at(iB) = eventInfo->auxdecor< float >( thisBTagName.c_str() );
    }

    thisBTagName = "mbj_"+baseBTagType+m_bTagWPNums.at(iB);
    if( eventInfo->isAvailable< float >( thisBTagName.c_str() ) )
      m_mbj.at(iB) = eventInfo->auxdecor< float >( thisBTagName.c_str() );

    thisBTagName = "weight_BTag_"+baseBTagType+m_bTagWPNums.at(iB);
    if( eventInfo->isAvailable< std::vector< float > >( thisBTagName.c_str() ) )
      m_weight_btag.at(iB) = eventInfo->auxdecor< std::vector< float > >( thisBTagName.c_str() );
  }


  // get vector of string giving the syst names of the upstream algo from TStore
  // (rememeber: 1st element is a blank string: nominal case!)
  if( m_firstEvent){
    std::vector< std::string >* systNames(nullptr);
    if( m_store->contains<std::vector<std::string> >( "BJetEfficiency_Algo_FixedCutBEff_85" )){
      HelperFunctions::retrieve(systNames, "BJetEfficiency_Algo_FixedCutBEff_85", m_event, m_store); //.isSuccess()
      std::cout << " Using " <<  systNames->size() << " systematics from BJetEfficiency_Algo_FixedCutBEff_85 " << std::endl;
    }else if( m_store->contains<std::vector<std::string> >( "BJetEfficiency_Algo_FixedCutBEff_77" )){
      HelperFunctions::retrieve(systNames, "BJetEfficiency_Algo_FixedCutBEff_77", m_event, m_store); //.isSuccess()
      std::cout << " Using " << systNames->size() << " systematics from BJetEfficiency_Algo_FixedCutBEff_77 " << std::endl;
    } else {
      std::cout << " No b-tagging systematics found !!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    }

    m_systSF_btag_names = *systNames;
    if( m_systSF_btag_names.size() > 0 && m_systSF_btag_names.at(0).size() == 0)
      m_systSF_btag_names.at(0) = "Nominal";

    m_firstEvent = false;
  }
}

void MiniTree::ClearBtag() {
  // Clear each WP's data //
  for( unsigned int iB=0; iB < m_bTagWPs.size(); ++iB){
    m_mbb.at(iB) = -999;
    m_mbj.at(iB) = -999;
    m_weight_btag.at(iB).clear();
  }
  m_systSF_btag_names.clear();
}
