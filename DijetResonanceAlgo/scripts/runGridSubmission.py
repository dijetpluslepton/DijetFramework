#!/usr/bin/env python

#####################################
# A replacement for run_grid.py
# Submit grid jobs using text files with lists of containers
# For questions contact Jeff.Dandoy@cern.ch
#####################################

import os, math, sys
from time import strftime

def printHelp() :
  print "Usage: runGridSubmission.py [runOn]"
  print "where `runOn' is one of"
  print "(data)\t\tdata15repro, data16, data16debug, data16extra, data17, data18"
  print "(backgrounds)\tmc16a_stdm4_qcdpythia, mc16a_stdm4_ttbar, mc16d_stdm4_ttbar, mc16a_stdm4_wpjets, mc16d_stdm4_wpjets, qcdpythia, qcdsherpa, qcdpowpythia, qcdherwig, qcdpowherwig, ttbar, wpjets"
  print "signal\t\twzprime, tc, Hplus4FS"

test = False # does not run the jobs
if not test:
  if not os.path.exists("gridOutput"):
    os.system("mkdir gridOutput")
  if not os.path.exists("gridOutput/gridJobs"):
    os.system("mkdir gridOutput/gridJobs")

#### Driver options ####
runType = 'grid'   #CERN grid
#runType = 'local'
#runType = 'condor' #Uchicago Condor

#### Set only for exotics group production rights ####
#production_name = "phys-exotics"
production_name = ""

outputTags = []
filesIn = []
configFiles = []

######### Configs #########
# data15
config15_Nominal_lepton         = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config15_Nominal_lepton.py"
# data16
config16_BackgroundSyst_lepton  = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config16_BackgroundMC_lepton.py"
config16_SignalSyst_lepton      = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config16_SignalSyst.py"
config16_Nominal_lepton         = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config16_Nominal_lepton.py"
# data17
config17_Nominal_lepton         = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config17_Nominal_lepton.py"
# data18
config18_Nominal_lepton         = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config18_Nominal_lepton.py"
# all data(15-18)
config_data_Nominal_lepton      = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config_data_Nominal_lepton.py"
# Background MC16a
config_mc16a_background         = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config_mc16a_background.py"
# Background MC16d
config_mc16d_background         = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config_mc16d_background.py"
# Signal MC16
config_SignalSyst_lepton        = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config_SignalSyst.py"
config_mc16a_SignalSyst_lepton  = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config_SignalSyst_mc16a.py"
config_mc16d_SignalSyst_lepton  = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config_SignalSyst_mc16d.py"
# Signal MC16 AFII
config_SignalSyst_mc16a_AFII  = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config_SignalSyst_mc16a_AFII.py"
config_SignalSyst_mc16d_AFII  = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config_SignalSyst_mc16d_AFII.py"
config_SignalSyst_mc16e_AFII  = "$DijetFramework_DIR/data/DijetResonanceAlgo/config/config_SignalSyst_mc16e_AFII.py"

if sys.argv[1] == "help" or sys.argv[1] == "-h" or sys.argv[1] == "--help" :
  printHelp()
  sys.exit(0)

runOn = sys.argv[1]

######### Data Options ####
if runOn == "data15repro" :
  outputTags.append("Main")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/Data13TeV_Main_EXOT2_gridSamples_2015repro.txt")
  configFiles.append( config_Nominal )
  # data15 individual runs
elif runOn == "data15" :
  outputTags.append("v2")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/data15_13TeV.physics_Main.deriv.STDM4.txt")
  configFiles.append( config_data_Nominal_lepton )
  # data15 PhysCont
elif runOn == "data15_PhysCont" :
  outputTags.append("v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/data15_13TeV.physics_Main.PhysCont.STDM4.txt")
  configFiles.append( config_data_Nominal_lepton )
  # data16 individual runs
elif runOn == "data16" :
  outputTags.append("v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/data16_13TeV.physics_Main.deriv.STDM4.txt")
  configFiles.append( config_data_Nominal_lepton )
  # data16 PhysCont
elif runOn == "data16_PhysCont" :
  outputTags.append("v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/data16_13TeV.physics_Main.PhysCont.STDM4.txt")
  configFiles.append( config_data_Nominal_lepton )
  # data17 individual runs
elif runOn == "data17" :
  outputTags.append("v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/data17_13TeV.physics_Main.deriv.STDM4.txt")
  configFiles.append( config_data_Nominal_lepton )
  # data17 PhysCont
elif runOn == "data17_PhysCont" :
  outputTags.append("v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/data17_13TeV.physics_Main.PhysCont.STDM4.txt")
  configFiles.append( config_data_Nominal_lepton )
  # data18 individual runs
elif runOn == "data18" :
  outputTags.append("v5")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/data18_13TeV.physics_Main.deriv.STDM4.txt")
  configFiles.append( config_data_Nominal_lepton )
elif runOn == "data18_singlerun_test" :
  outputTags.append("iso_uncertcalib_bj")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/data18_singlerun_test.txt")
  configFiles.append( config_data_Nominal_lepton )
  # data18 PhysCont
elif runOn == "data16extra" :
  outputTags.append("v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/Data13TeV_Main_JETM2_gridSamples_2016_lepton_extra.txt")
  configFiles.append( config_Nominal_2016_lepton )
elif runOn == "data16debug" :
  # debugging using 1 file
  outputTags.append("Debug")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/Data13TeV_Debug_gridSamples.txt")
  configFiles.append( config_Nominal_2016_lepton  )

### For broken jobs -- MUST ALWAYS BE UPDATED! ###
elif runOn == "broken_data" :
  outputTags.append("v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/broken_data_20181205.txt")
  configFiles.append( config_data_Nominal_lepton )


##### QCD Options #####
elif runOn == "mc16a_stdm4_qcdpythia" :
  outputTags.append("mc16a_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16a_QCDPythia8_STDM4.txt")
  configFiles.append( config_mc16a_background )

elif runOn == "mc16d_stdm4_qcdpythia" :
  outputTags.append("mc16d_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16d_QCDPythia8_STDM4.txt")
  configFiles.append( config_mc16d_background )

elif runOn == "qcdpythia" :
  outputTags.append("QCDPythia8_dijetlep_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/QCDPythia8_EXOT2_gridSamples.txt")
  configFiles.append( config_BackgroundSyst_lepton )
elif runOn == "qcdpythia_r21" :
  outputTags.append("QCDPythia8_dijetlep_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/PowhegPythia8_2017R21.txt")
  configFiles.append( config_BackgroundSyst_lepton )

# do not use this:
# filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/QCDPythia8_gridSamples.txt")

elif runOn == "qcdsherpa" :
  outputTags.append("Sherpa")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/QCDSherpa_gridSamples.txt")
  configFiles.append( config_BackgroundSyst )
elif runOn == "qcdpowpythia" :
  outputTags.append("PowhegPythia")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/QCDPowhegPythia_50_gridSamples.txt")
  configFiles.append( config_BackgroundSyst )
elif runOn == "qcdherwig" :
  outputTags.append("Herwig")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/QCDHerwig_EXOT2_gridSamples.txt")
  ##filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/QCDHerwig_gridSamples.txt")
  configFiles.append( config_BackgroundSyst )
elif runOn == "qcdpowherwig" :
  outputTags.append("PowhegHerwig")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/QCDPowhegHerwig_gridSamples.txt")
  configFiles.append( config_BackgroundSyst )

### Signal WZprime
elif runOn == "wzprime" :
  outputTags.append("mc15c_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/SignalWZprime_DAOD.txt")
  configFiles.append(  config_SignalSyst_lepton    )
  
elif runOn == "wzprime_mc16a" :
  outputTags.append("mc16a_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16a_Wprime_STDM4.txt")
  configFiles.append(  config_mc16a_SignalSyst_lepton    )
  
elif runOn == "wzprime_mc16d" :
  outputTags.append("mc16d_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16d_Wprime_STDM4.txt")
  configFiles.append(  config_mc16d_SignalSyst_lepton    )

### Signal SimpZprime
elif runOn == "simpZprime_mc16a_AFII" :
  outputTags.append("mc16a_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16a_SimpZprime_STDM4.txt")
  configFiles.append(  config_SignalSyst_mc16a_AFII    )

elif runOn == "simpZprime_mc16d_AFII" :
  outputTags.append("mc16d_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16d_SimpZprime_STDM4.txt")
  configFiles.append(  config_SignalSyst_mc16d_AFII    )

elif runOn == "simpZprime_mc16e_AFII" :
  outputTags.append("mc16e_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16e_SimpZprime_STDM4.txt")
  configFiles.append(  config_SignalSyst_mc16e_AFII    )

### Signal Techicolor Rho_Pi 
elif runOn == "tc_mc16a" :
  outputTags.append("mc16a_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16a_TC_STDM4.txt")
  configFiles.append(  config_SignalSyst_lepton   )

elif runOn == "tc" :
  outputTags.append("rhopi_dijetlep_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/SignalRhopi_DAOD.txt")
  configFiles.append(  config_SignalSyst_lepton   )

### Signal Charged Higgs 4FS tb
elif runOn == "Hplus4FS_mc16a" :
  outputTags.append("mc16a_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16a_Hplus4FS_STDM4.txt")
  configFiles.append(  config_SignalSyst_lepton   )
elif runOn == "Hplus4FS_mc16e" :
  outputTags.append("mc16e_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16e_Hplus4FS_STDM4.txt")
  configFiles.append(  config_SignalSyst_mc16e_AFII   )

######ttbar#######
elif runOn == "mc16a_stdm4_ttbar" :
  outputTags.append("mc16a_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16a_ttbar_STDM4.txt")
  configFiles.append( config_mc16a_background )
elif runOn == "mc16d_stdm4_ttbar" :
  outputTags.append("mc16d_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16d_ttbar_STDM4.txt")
  configFiles.append( config_mc16d_background )
elif runOn == "mc16d_topq1_singletop" :
  outputTags.append("mc16d_v3")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16d_singletop_TOPQ1.txt")
  configFiles.append( config_mc16d_background )
elif runOn == "ttbar" :
  outputTags.append("ttbar_dijetlep_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/ttbar_FTAG2_gridSamples_r20.7.txt")
  configFiles.append(  config_BackgroundSyst_lepton )

###### Z+jet #########
elif runOn == "mc16a_stdm4_zpjets" :
  outputTags.append("mc16a_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16a_Zpjet_STDM4.txt")
  configFiles.append( config_mc16a_background )
elif runOn == "mc16d_stdm4_zpjets" :
  outputTags.append("mc16d_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16d_Zpjet_STDM4.txt")
  configFiles.append( config_mc16d_background )

###### W+jet #########
elif runOn == "mc16a_stdm4_wpjets" :
  outputTags.append("mc16a_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16a_Wpjet_STDM4.txt")
  configFiles.append( config_mc16a_background )
elif runOn == "mc16d_stdm4_wpjets" :
  outputTags.append("mc16d_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/sampleLists/mc16d_Wpjet_STDM4.txt")
  configFiles.append( config_mc16d_background )
elif runOn == "wpjets" :
  outputTags.append("PowhegPythia_dijetlep_v1")
  filesIn.append("$DijetFramework_DIR/data/DijetResonanceAlgo/WPlusJet_gridSamples.txt")
  configFiles.append( config_BackgroundSyst_lepton )

###### sample not found ######
else :
  print "runGridSubmission-- Unrecognised input!"
  printHelp()
  sys.exit(0)


#### Signal Options #######
  #outputTags.append("WPrime")
  #filesIn.append( "$DijetFramework_DIR/data/DijetResonanceAlgo/WPrimeQQ_EXOT2_gridSamples.txt" )
  ###filesIn.append( "$DijetFramework_DIR/data/DijetResonanceAlgo/WPrimeQQ_gridSamples.txt" )
  #configFiles.append( config_SignalSyst )
  #
  #outputTags.append("ZP")
  ##outputTags.append("ZPrimeQQ")
  #filesIn.append( "$DijetFramework_DIR/data/DijetResonanceAlgo/ZPrimeQQ_gridSamples.txt" )
  #configFiles.append( config_SignalSyst )

######## AFII Fast sim!!! Needs config file changes !! ######
#outputTags.append("QBH_50")
#filesIn.append( "$DijetFramework_DIR/data/DijetResonanceAlgo/QBHADD_EXOT2_Fastsim_gridSamples.txt" )
####filesIn.append( "$DijetFramework_DIR/data/DijetResonanceAlgo/QBHADD_Fastsim_gridSamples.txt" )
#configFiles.append( config_SignalSyst )
#
#outputTags.append("QBH_50")
#filesIn.append( "$DijetFramework_DIR/data/DijetResonanceAlgo/QBHRS_Fastsim_gridSamples.txt" )
#configFiles.append( config_SignalSyst )


timestamp = strftime("_%Y%m%d")
# timestamp ="_20171215"


### Run on each input text file ###
for iFile, file_in in enumerate(filesIn):

  output_tag = outputTags[iFile]
  output_tag += timestamp

  ##Systematic information is in the config file name
  syst_tag = configFiles[iFile].split('_')[-1].replace('.py','')
  this_output_tag = syst_tag+"."+output_tag

  ## submission directory for storing records ##
  submit_dir = "gridOutput/gridJobs/submitDir_"+os.path.basename(file_in).rsplit('.',1)[0]+"."+this_output_tag # define submit dir name

  ## Configure submission driver ##
  driverCommand = ''
  if runType == 'grid':
    # driverCommand = 'prun --optGridDestSE=ANL-ATLAS-GRIDFTP1 --optGridOutputSampleName='
    # driverCommand = 'prun --optGridOutputSampleName='
    driverCommand = 'prun --optGridMaxNFilesPerJob=4 --optGridNFilesPerJob=4 --optGridNGBPerJob=4 --optGridOutputSampleName='
    if len(production_name) > 0:
      driverCommand = ' prun --optSubmitFlags="--official" --optGridOutputSampleName='
      driverCommand += 'group.'+production_name
    else:
      driverCommand += 'user.%nickname%'
    driverCommand += '.%in:name[1]%.%in:name[2]%.%in:name[3]%.'+output_tag
  elif runType == 'condor':
    driverCommand = ' condor --optFilesPerWorker 10 --optBatchWait'
  elif runType == 'local':
    driverCommand = ' direct'

  ## create and submit command ##
  command = '$DijetFramework_DIR/bin/xAH_run.py'
  if runType == 'grid':
    command += ' --inputRucio'
#    command += ' --inputDQ2' <-- deprecated
  command += ' --inputList --files '+file_in
  command += ' --config '+configFiles[iFile]
  command += ' --force --submitDir '+submit_dir
  command += ' '+driverCommand

  print command
  if not test: os.system(command)
