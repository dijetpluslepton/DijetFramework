import ROOT
from xAODAnaHelpers import Config

c = Config()
# Use strings for long directory names to prevent word-wrapping algorithm configs
grlXML="GoodRunsLists/data18_13TeV/20181111/physics_25ns_Triggerno17e33prim.xml"
lumicalc="GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root"

prw_SimpZW="$DijetFramework_DIR/data/DijetResonanceAlgo/prw/simpZ/mc16_13TeV.MGPy8EG_A14NNPDF23LO_DMsimp_A1_WZprime.deriv.NTUP_PILEUP.e7056_a875_r10724_p3604.root"
prw_SimpZZ="$DijetFramework_DIR/data/DijetResonanceAlgo/prw/simpZ/mc16_13TeV.MGPy8EG_A14NNPDF23LO_DMsimp_A1_ZZprime.deriv.NTUP_PILEUP.e7056_a875_r10724_p3604.root"
prw=prw_SimpZW+","+prw_SimpZZ

#%%%%%%%%%%%%%%%%%%%%%%%%%% BasicEventSelection %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("BasicEventSelection",    { 
  "m_name"                      : "BasicEventSelect",
  "m_applyGRLCut"               : True,
  "m_GRLxml"                    : grlXML,
  "m_derivationName"            : "STDM4",
  "m_useMetaData"               : False,
  "m_storePassHLT"              : True,
  "m_storeTrigDecisions"        : True,
  "m_storePassL1"	              : True,
  "m_storeTrigKeys" 	          : True,
  "m_applyTriggerCut"           : True,
  "m_triggerSelection"          : "HLT_mu24_i.*|HLT_mu26_i.*|HLT_mu40|HLT_mu50|L1_MU15|L1_MU20|L1_2MU6|L1_MU6_J20|L1_MU10_2J20|HLT_2mu14|HLT_mu22_mu8noL1|HLT_2mu10|HLT_mu20_mu8noL1|HLT_e26_lhtight_nod0_ivarloose|HLT_e60_lhmedium_nod0|HLT_e60_medium|HLT_e140_lhloose_nod0|HLT_e300_etcut",
  "m_checkDuplicatesData"       : False,
  "m_doPUreweighting"           : True,
  "m_autoconfigPRW"             : False,
  "m_lumiCalcFileNames"         : lumicalc,
  "m_PRWFileNames"              : prw,
  "m_applyEventCleaningCut"     : True,
  "m_applyCoreFlagsCut"	        : True,
  "m_vertexContainerName"       : "PrimaryVertices",
  "m_applyPrimaryVertexCut"     : True, 
  "m_PVNTrack"		              : 2,
  "m_mcCampaign"                : "MC16a",
  "m_msgLevel"                  : "Info",
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% JetCalibrator %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("JetCalibrator",     {
  "m_name"                      : "JetCalibrate",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "AntiKt4EMTopoJets",
  "m_jetAlgo"                   : "AntiKt4EMTopo",
  "m_outContainerName"          : "AntiKt4EMTopoJets_Calib",
  "m_outputAlgo"                : "AntiKt4EMTopoJets_Calib_Algo",
  "m_sort"                      : True,
  "m_redoJVT"                   : True,
  #----------------------- Systematics ----------------------------#
  "m_systName"                  : "All",
  "m_systVal"                   : 1,
  #----------------------- Calibration ----------------------------#
  # https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ApplyJetCalibrationR21
  "m_setAFII"                   : True,
  "m_calibSequence"             : "JetArea_Residual_EtaJES_GSC",
  "m_forceInsitu"               : False, # Insitu calibration file not found in some cases
  #----------------------- Uncertainty ----------------------------#
  "m_overrideUncertCalibArea"   : "CalibArea-06",
  "m_uncertConfig"              : "rel21/Fall2018/R4_SR_Scenario1_SimpleJER.config",
  "m_uncertMCType"              : "MC16",
  #----------------------- JES Uncertainty ----------------------------#
  #"m_JESCalibArea"              : "CalibArea-05
  #"m_JESUncertConfig"           : "rel21/Summer2018/R4_StrongReduction_Scenario1_SimpleJER.config",
  #"m_JESUncertMCType"           : "MC16",
  #----------------------- JER Uncertainty ----------------------------#
  #"m_JERUncertConfig"           : "JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root",
  #"m_JERFullSys"                : False,
  #"m_JERApplyNominal"           : False,
  #----------------------- Cleaning ----------------------------#
  "m_jetCleanCutLevel"          : "LooseBad",
  "m_jetCleanUgly"              : True,
  "m_saveAllCleanDecisions"     : True,
  "m_cleanParent"               : False,
  #----------------------- Other ----------------------------#
  "m_addGhostMuonsToJets"       : True,
  "m_msgLevel"                  : "Info",
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% JetSelector %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("JetSelector",     {
  "m_name"                      : "JetSelect",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "AntiKt4EMTopoJets_Calib",
  "m_outContainerName"          : "Jets_Selected",
  "m_inputAlgo"                 : "AntiKt4EMTopoJets_Calib_Algo",
  "m_outputAlgo"                : "Jets_Selected_Algo",
  "m_decorateSelectedObjects"   : True,
  "m_createSelectedContainer"   : True,
  #----------------------- Selections ----------------------------#
  "m_cleanJets"                 : True,
  "m_pass_min"                  : 2,
  "m_pT_min"                    : 20e3,
  "m_eta_max"                   : 5,
  #----------------------- JVT ----------------------------#
  "m_doJVT"                     : True,
  "m_pt_max_JVT"                : 60e3,
  "m_eta_max_JVT"               : 2.4,
  "m_JVTCut"                    : 0.59,
  #----------------------- B-tagging ----------------------------#
  "m_doBTagCut"                 : False,
  #----------------------- Other ----------------------------#
  "m_msgLevel"                  : "Info",
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% MuonCalibrator %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("MuonCalibrator",     {
  "m_name"                      : "MuonCalib",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "Muons",
  "m_outContainerName"          : "Muons_Calib",
  #----------------------- Systematics ----------------------------#
  "m_systName"                  : "All",
  "m_systVal"                   : 0,
  #----------------------- Other ----------------------------#
  "m_sort"                      : True,
  "m_msgLevel"                  : "Info"
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% MuonSelector %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("MuonSelector",     {
  "m_name"                      : "MuonSelect",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "Muons_Calib",
  "m_outContainerName"          : "Muons_Selected",
  "m_createSelectedContainer"   : True,
  #----------------------- Systematics ----------------------------#
  "m_systName"                  : "",        ## Data
  "m_systVal"                   : 0,
  #----------------------- configurable cuts ----------------------------#
  "m_muonQualityStr"            : "VeryLoose",
  "m_pass_max"                  : -1,
  "m_pass_min"                  : -1,
  "m_pT_max"                    : 1e8,
  "m_pT_min"                    : 20e3,
  "m_eta_max"                   : 1e8,
  "m_d0_max"                    : 1e8,
  "m_d0sig_max"     	        : 1e8,
  "m_z0sintheta_max"            : 1e8,
  #----------------------- isolation stuff ----------------------------#
  "m_MinIsoWPCut"               : "",
  "m_IsoWPList"                 : "FCTightTrackOnly,FixedCutHighPtTrackOnly,FCLoose,FCTight",
  #----------------------- trigger matching stuff ----------------------------#
  "m_singleMuTrigChains"        : "HLT_mu50,HLT_mu40,HLT_mu26_imedium,HLT_mu26_ivarmedium,HLT_mu24_imedium,HLT_mu24_ivarmedium,HLT_mu24_ivarloose,HLT_mu24_iloose",
  "m_minDeltaR"                 : 0.1,
  #----------------------- Other ----------------------------#
  "m_msgLevel"                  : "Info"
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% ElectronCalibrator %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("ElectronCalibrator",     {
  "m_name"                      : "ElectronCalib",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "Electrons",
  "m_outContainerName"          : "Electrons_Calib",
  #----------------------- Systematics ----------------------------#
  "m_systName"                  : "All",            ## For data
  "m_systVal"                   : 0,                    ## For data
  "m_esModel"                   : "es2017_R21_v1",
  "m_decorrelationModel"        : "1NP_v1",
  #----------------------- Other ----------------------------#
  "m_sort"                      : True,
  "m_msgLevel"                  : "Info"
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% ElectronSelector %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("ElectronSelector",     {
  "m_name"                      : "ElectronSelect",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "Electrons_Calib",
  "m_outContainerName"          : "Electrons_Selected",
  "m_createSelectedContainer"   : True,
  #----------------------- configurable cuts ----------------------------#
  "m_pass_max"                  : -1,
  "m_pass_min"                  : -1,
  "m_pT_max"                    : 1e8,
  "m_pT_min"                    : 20e3,
  "m_eta_max"                   : 1e8,
  "m_d0_max"                    : 1e8,
  "m_d0sig_max"     	          : 1e8,
  "m_z0sintheta_max"            : 1e8,
  #----------------------- isolation stuff ----------------------------#
  "m_MinIsoWPCut"               : "",
  "m_IsoWPList"                 : "FCHighPtCaloOnly,Gradient,FCLoose,FCTight",
  #----------------------- ID ----------------------------#
  ### needed for new derivations:
  ### https://groups.cern.ch/group/hn-atlas-EGammaWG/Lists/Archive/Flat.aspx?RootFolder=%2Fgroup%2Fhn-atlas-EGammaWG%2FLists%2FArchive%2FMissing%20likelihood%20variables%20in%20default%20derivation%20lists&FolderCTID=0x01200200D17DAC8476E41D4F8451088E75A5CE34
  "m_readIDFlagsFromDerivation" : True,
  #----------------------- trigger matching stuff ----------------------------#
  "m_singleElTrigChains"        : "HLT_e60_medium,HLT_e60_lhmedium_nod0,HLT_e26_lhtight_nod0_ivarloose,HLT_e24_lhmedium_nod0_ivarloose",
  #----------------------- Other ----------------------------#
  "m_msgLevel"                  : "Info",
})

  #%%%%%%%%%%%%%%%%%%%%%%%%%% METConstructor %%%%%%%%%%%%%%%%%%%%%%%%%%#
  # Must be done before overlap removal
c.algorithm("METConstructor",     {
  "m_name"                      : "DijetLepton_METConstructor",
  "m_referenceMETContainer"     : "MET_Reference_AntiKt4EMTopo",
  "m_mapName"                   : "METAssoc_AntiKt4EMTopo",
  "m_coreName"                  : "MET_Core_AntiKt4EMTopo",
  "m_outputContainer"           : "MET_rebuilt",
  #----------------------- MET objects ----------------------------#
  "m_inputElectrons"	        : "Electrons_Selected",
  "m_inputMuons"		: "Muons_Selected",
  "m_inputJets"		        : "AntiKt4EMTopoJets_Calib",
  #----------------------- MET config ----------------------------#
  "m_rebuildUsingTracksInJets"  : False,
  "m_addSoftClusterTerms"       : True,
  "m_doJVTCut"	                : True,
  "m_doMuonEloss"               : True,
  "m_doIsolMuonEloss"           : True,
    #----------------------- Other ----------------------------#
  "m_msgLevel"		        : "Info"
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% BJetEfficiencyCorrector %%%%%%%%%%%%%%%%%%%%%%%%%%#
bJetWPs = ["FixedCutBEff_60", "FixedCutBEff_70", "FixedCutBEff_77", "FixedCutBEff_85"]
#bJetTaggers = ["DL1", "DL1r", "DL1rmu", "MV2c10", "MV2r", "MV2rmu"]
bJetTaggers = ["DL1", "MV2c10"]

for bJetTagger in bJetTaggers:
  for bJetWP in bJetWPs:
    c.algorithm("BJetEfficiencyCorrector",     {
      "m_name"                    : bJetWP,
      #----------------------- Container Flow ----------------------------#
      "m_inContainerName"         : "Jets_Selected",
      "m_jetAuthor"               : "AntiKt4EMTopoJets",
      "m_decor"                   : "BTag",
      "m_outputSystName"          : "BJetEfficiency_Algo",
      #----------------------- B-tag Options ----------------------------#
      "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root",
      "m_taggerName"              : bJetTagger,
      "m_operatingPt"             : bJetWP,
      "m_coneFlavourLabel"        : True,
      "m_useDevelopmentFile"      : True,
      #----------------------- Other ----------------------------#
      "m_msgLevel"                : "Info"
})

##%%%%%%%%%%%%%%%%%%%%%%%%%% DijetResonanceAlgo %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("ResonanceAlgorithm",     {
    "m_name"                    : "ResonanceAlgo",
    #----------------------- Container Flow ----------------------------#
    "m_inJetContainerName"      : "Jets_Selected",
    "m_inMuContainerName"       : "Muons_Selected",
    "m_inElContainerName"       : "Electrons_Selected",
    "m_inputAlgo"               : "Jets_Selected_Algo",
    "m_allJetContainerName"     : "AntiKt4EMTopoJets_Calib",
    "m_allJetInputAlgo"         : "AntiKt4EMTopoJets_Calib_Algo",
    #----------------------- Selections ----------------------------#
    "m_leadingJetPtCut"         : 0,
    #----------------------- Output ----------------------------#
    "m_doBtag"                  : False,
    "m_eventDetailStr"          : "truth pileup", #shapeEM
    "m_jetDetailStr"            : "kinematic rapidity clean energy truth flavorTag trackAll trackPV allTrack allTrackPVSel allTrackDetail allTrackDetailPVSel btag_jettrk", # no `truth_details` do to missing aux `::GhostParton`
    "m_jetDetailStrSyst"        : "kinematic rapidity energy clean flavorTag",
    "m_elDetailStr"             : "kinematic clean energy truth flavorTag isolation trackparams trackhitcont effSF PID ISOL_FCHighPtCaloOnly ISOL_Gradient ISOL_FCLoose ISOL_FCTight PID_LHLoose PID_LHLooseBL PID_LHMedium PID_LHTight trigger TRIG_HLT_e26_lhtight_nod0_ivarloose TRIG_HLT_e24_lhmedium_nod0_ivarloose TRIG_HLT_e60_medium TRIG_HLT_e60_lhmedium_nod0",
    "m_muDetailStr"             : "kinematic clean energy truth flavorTag isolation trackparams trackhitcont effSF quality energyLoss trigger ISOL_FixedCutHighPtTrackOnly ISOL_FCTightTrackOnly ISOL_FCLoose ISOL_FCTight RECO_HighPt RECO_LowPt RECO_Loose RECO_Medium RECO_Tight TRIG_HLT_mu24_iloose TRIG_HLT_mu24_ivarloose TRIG_HLT_mu24_ivarmedium TRIG_HLT_mu24_imedium TRIG_HLT_mu26_ivarmedium TRIG_HLT_mu26_imedium TRIG_HLT_mu40 TRIG_HLT_mu50",
    "m_trigDetailStr"           : "basic menuKeys passTriggers",
    #----------------------- Other ----------------------------#
    "m_writeTree"               : True,
    "m_MCPileupCheckContainer"  : "AntiKt4TruthJets",
    "m_msgLevel"                : "Info"
})


