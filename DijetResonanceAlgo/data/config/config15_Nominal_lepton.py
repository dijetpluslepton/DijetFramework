import ROOT
from xAODAnaHelpers import Config

c = Config()

#%%%%%%%%%%%%%%%%%%%%%%%%%% BasicEventSelection %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("BasicEventSelection",    { 
  "m_name"                      : "BasicEventSelect",
  "m_applyGRLCut"               : True,
  "m_GRLxml"                    : "GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
  "m_derivationName"            : "JETM2",
  "m_useMetaData"               : False,
  "m_storePassHLT"              : True,
  "m_storeTrigDecisions"        : True,
  "m_storePassL1"	        : True,
  "m_storeTrigKeys" 	        : True,
  "m_applyTriggerCut"           : True,
   "m_triggerSelection"         : "HLT_mu24_i.*|HLT_mu26_i.*|HLT_mu40|HLT_mu50|L1_MU15|L1_MU20|L1_2MU6|L1_MU6_J20|L1_MU10_2J20|HLT_2mu14|HLT_mu22_mu8noL1|HLT_2mu10|HLT_mu20_mu8noL1|HLT_e26_lhtight_nod0_ivarloose|HLT_e60_lhmedium_nod0|HLT_e60_medium|HLT_e140_lhloose_nod0|HLT_e300_etcut",
  "m_checkDuplicatesData"       : False,
  "m_applyEventCleaningCut"     : False,
  "m_applyCoreFlagsCut"	        : True,
  "m_vertexContainerName"       : "PrimaryVertices",
  "m_applyPrimaryVertexCut"     : True, 
  "m_PVNTrack"		        : 2,
  "m_msgLevel"                  : "Info",
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% JetCalibrator %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("JetCalibrator",     {
  "m_name"                      : "JetCalibrate",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "AntiKt4EMTopoJets",
  "m_jetAlgo"                   : "AntiKt4EMTopo",
  "m_outContainerName"          : "AntiKt4EMTopoJets_Calib",
  "m_outputAlgo"                : "AntiKt4EMTopoJets_Calib_Algo",
  "m_sort"                      : True,
  "m_redoJVT"                   : True,
  #----------------------- Systematics ----------------------------#
  "m_systName"                  : "Nominal",            ## For data
  "m_systVal"                   : 0,                    ## For data
  #----------------------- Calibration ----------------------------#
  "m_calibConfigAFII"           : "JES_MC15Prerecommendation_AFII_June2015.config",
  "m_calibConfigFullSim"        : "JES_MC16Recommendation_28Nov2017.config",
  "m_calibConfigData"           : "JES_MC16Recommendation_28Nov2017.config",
  "m_calibSequence"             : "JetArea_Residual_EtaJES_GSC",
  "m_forceInsitu"               : False, # Insitu calibration file not found in some cases
  #----------------------- JES Uncertainty ----------------------------#
  "m_JESUncertConfig"           : "DijetResonanceAlgo/JES2016_SR_Scenario1.config",
  "m_JESUncertMCType"           : "MC15",
  #----------------------- JER Uncertainty ----------------------------#
  "m_JERUncertConfig"           : "JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root",
  "m_JERFullSys"                : False,
  "m_JERApplyNominal"           : False,
  #----------------------- Cleaning ----------------------------#
  "m_jetCleanCutLevel"          : "LooseBad",
  "m_jetCleanUgly"              : False,
  "m_saveAllCleanDecisions"     : True,
  "m_cleanParent"               : False,
  #----------------------- Other ----------------------------#
  "m_msgLevel"                  : "Info",
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% JetSelector %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("JetSelector",     {
  "m_name"                      : "JetSelect",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "AntiKt4EMTopoJets_Calib",
  "m_outContainerName"          : "Jets_Selected",
  "m_inputAlgo"                 : "AntiKt4EMTopoJets_Calib_Algo",
  "m_outputAlgo"                : "Jets_Selected_Algo",
  "m_decorateSelectedObjects"   : True,
  "m_createSelectedContainer"   : True,
  #----------------------- Selections ----------------------------#
  "m_cleanJets"                 : False,
  "m_pass_min"                  : 2,
  "m_pT_min"                    : 20e3,
  "m_eta_max"                   : 5,
  #----------------------- JVT ----------------------------#
  "m_doJVT"                     : False,
  "m_pt_max_JVT"                : 60e3,
  "m_eta_max_JVT"               : 2.4,
  "m_JVTCut"                    : 0.59,
  #----------------------- B-tagging ----------------------------#
  "m_doBTagCut"                 : False,
  #----------------------- Other ----------------------------#
  "m_msgLevel"                  : "Info",
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% MuonCalibrator %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("MuonCalibrator",     {
  "m_name"                      : "MuonCalib",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "Muons",
  "m_outContainerName"          : "Muons_Calib",
  #----------------------- Systematics ----------------------------#
  "m_systName"                  : "",
  "m_systVal"                   : 0,
  #----------------------- Other ----------------------------#
  "m_sort"                      : True,
  "m_msgLevel"                  : "Info"
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% MuonSelector %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("MuonSelector",     {
  "m_name"                      : "DijetLepton_MuonSelector",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "Muons_Calib",
  "m_outContainerName"          : "Muons_Selected",
  "m_createSelectedContainer"   : True,
  #----------------------- Systematics ----------------------------#
  "m_systName"                  : "",        ## Data
  "m_systVal"                   : 0,
  #----------------------- configurable cuts ----------------------------#
  "m_muonQualityStr"            : "VeryLoose",
  "m_pass_max"                  : -1,
  "m_pass_min"                  : -1,
  "m_pT_max"                    : 1e8,
  "m_pT_min"                    : 20e3,
  "m_eta_max"                   : 1e8,
  "m_d0_max"                    : 1e8,
  "m_d0sig_max"     	        : 1e8,
  "m_z0sintheta_max"            : 1e8,
  #----------------------- isolation stuff ----------------------------#
  "m_MinIsoWPCut"               : "",
  #----------------------- trigger matching stuff ----------------------------#
  "m_singleMuTrigChains"        : "HLT_mu50",
  "m_minDeltaR"                 : 0.1,
  #----------------------- Other ----------------------------#
  "m_msgLevel"                  : "Info"
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% ElectronCalibrator %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("ElectronCalibrator",     {
  "m_name"                      : "ElectronCalib",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "Electrons",
  "m_outContainerName"          : "Electrons_Calib",
  #----------------------- Systematics ----------------------------#
  "m_systName"                  : "Nominal",            ## For data
  "m_systVal"                   : 0,                    ## For data
  "m_esModel"                   : "es2016PRE",
  "m_decorrelationModel"        : "1NP_v1",
  #----------------------- Other ----------------------------#
  "m_sort"                      : True,
  "m_msgLevel"                  : "Info"
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% ElectronSelector %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("ElectronSelector",     {
  "m_name"                      : "ElectronSelect",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"           : "Electrons_Calib",
  "m_outContainerName"          : "Electrons_Selected",
  "m_createSelectedContainer"   : True,
  #----------------------- configurable cuts ----------------------------#
  "m_pass_max"                  : -1,
  "m_pass_min"                  : -1,
  "m_pT_max"                    : 1e8,
  "m_pT_min"                    : 20e3,
  "m_eta_max"                   : 1e8,
  "m_d0_max"                    : 1e8,
  "m_d0sig_max"     	        : 1e8,
  "m_z0sintheta_max"            : 1e8,
  #----------------------- isolation stuff ----------------------------#
  "m_MinIsoWPCut"               : "",
  #----------------------- trigger matching stuff ----------------------------#
  "m_singleElTrigChains"        : "",
  #----------------------- Other ----------------------------#
  "m_msgLevel"                  : "Info"
})

  #%%%%%%%%%%%%%%%%%%%%%%%%%% METConstructor %%%%%%%%%%%%%%%%%%%%%%%%%%#
  # Must be done before overlap removal
c.algorithm("METConstructor",     {
  "m_name"                      : "DijetLepton_METConstructor",
  "m_referenceMETContainer"     : "MET_Reference_AntiKt4EMTopo",
  "m_mapName"                   : "METAssoc_AntiKt4EMTopo",
  "m_coreName"                  : "MET_Core_AntiKt4EMTopo",
  "m_outputContainer"           : "MET_rebuilt",
  #----------------------- MET objects ----------------------------#
  "m_inputElectrons"	        : "Electrons_Selected",
  "m_inputMuons"		: "Muons_Selected",
  "m_inputJets"		        : "AntiKt4EMTopoJets_Calib",
  #----------------------- MET config ----------------------------#
  "m_rebuildUsingTracksInJets"  : False,
  "m_addSoftClusterTerms"       : True,
  "m_doJVTCut"	                : True,
  "m_doMuonEloss"               : True,
  "m_doIsolMuonEloss"           : True,
    #----------------------- Other ----------------------------#
  "m_msgLevel"		        : "Info"
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% BJetEfficiencyCorrector %%%%%%%%%%%%%%%%%%%%%%%%%%#
bJetWPs = ["FixedCutBEff_60", "FixedCutBEff_70", "FixedCutBEff_77", "FixedCutBEff_85"]

for bJetWP in bJetWPs:
  c.algorithm("BJetEfficiencyCorrector",     {
    "m_name"                    : bJetWP,
    #----------------------- Container Flow ----------------------------#
    "m_inContainerName"         : "Jets_Selected",
    "m_jetAuthor"               : "AntiKt4EMTopoJets",
    "m_decor"                   : "BTag",
    "m_outputSystName"          : "BJetEfficiency_Algo",
    #----------------------- B-tag Options ----------------------------#
    "m_corrFileName"            : "DijetResonanceAlgo/2016-20_7-13TeV-MC15-CDI-2016-11-25_v1.root",
    "m_taggerName"              : "MV2c10",
    "m_operatingPt"             : bJetWP,
    "m_coneFlavourLabel"        : True,
    "m_useDevelopmentFile"      : True,
    #----------------------- Other ----------------------------#
    "m_msgLevel"                : "Info"
})

##%%%%%%%%%%%%%%%%%%%%%%%%%% DijetResonanceAlgo %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("ResonanceAlgorithm",     {
    "m_name"                    : "ResonanceAlgo",
    #----------------------- Container Flow ----------------------------#
    "m_inJetContainerName"      : "Jets_Selected",
    "m_inMuContainerName"       : "Muons_Selected",
    "m_inElContainerName"       : "Electrons_Selected",
    "m_inputAlgo"               : "Jets_Selected_Algo",
    "m_allJetContainerName"     : "AntiKt4EMTopoJets_Calib",
    "m_allJetInputAlgo"         : "AntiKt4EMTopoJets_Calib_Algo",
    #----------------------- Selections ----------------------------#
    "m_leadingJetPtCut"         : 0,
    #----------------------- Output ----------------------------#
    "m_doBtag"                  : False,
    "m_eventDetailStr"          : "truth pileup", #shapeEM
    "m_jetDetailStr"            : "kinematic rapidity clean energy truth flavorTag trackAll trackPV allTrack allTrackPVSel allTrackDetail allTrackDetailPVSel btag_jettrk", # no `truth_details` do to missing aux `::GhostParton`
    "m_jetDetailStrSyst"        : "kinematic rapidity energy clean flavorTag",
    "m_elDetailStr"             : "kinematic clean energy truth flavorTag isolation trackparams trackhitcont effSF PID ISOL_ ISOL_GradientLoose ISOL_FixedCutLoose ISOL_FixedCutTight ISOL_Gradient ISOL_LooseTrackOnly ISOL_Loose ISOL_Medium ISOL_Tight PID_LHVeryLoose PID_LHLoose PID_LHLooseBL PID_LHMedium PID_LHTight PID_EMLoose PID_EMMedium PID_EMTight", #trigger 
    "m_muDetailStr"             : "kinematic clean energy truth flavorTag isolation trackparams trackhitcont effSF quality energyLoss ISOL_Loose ISOL_Tight ISOL_FixedCutLoose ISOL_GradientLoose ISOL_Gradient ISOL_LooseTrackOnly RECO_VeryLoose RECO_Loose RECO_Medium RECO_Tight", #trigger
    "m_trigDetailStr"           : "basic menuKeys passTriggers",
    #----------------------- Other ----------------------------#
    "m_writeTree"               : True,
    "m_MCPileupCheckContainer"  : "AntiKt4TruthJets",
    "m_msgLevel"                : "Info"
})


